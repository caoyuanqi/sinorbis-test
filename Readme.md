# Design
## technical framework
Spring boot is choosed as IOC container and MVC framework, aws jdk is imported for access for s3 service;
## Design
1. Be default this service will run on port 8080
2. client could post image files to this service with parameters 'images'
3. if post is success and no exception is rise, client will get get a list of file name which is in this format
    [
        0 :
        {
            originalName : "1.jpeg",
            small : "f93a056396bd4a9da4ed9cf436d2bc82.jpg",
            middle : "89f9d893f3094e259345ab4d0d32b090.jpg",
            large : "258b0d96a16941baa6573c828205ea4b.jpg"
        },
        1 :
        {
            originalName : "2.jpg",
            small : "b4388823d9514168914b3b9ea4817779.jpg",
            middle : "14e4c13557b64ae0a30008cbd59a9c8c.jpg",
            large : "00bb1fcdb9694a56b4a3a380ce9cc489.jpg"
        }
    ]
4. some exception will be raised inside for some exception: no file is uploaded, some file is not image file, inner error.
5. A test.html file is gaven for the the test 
6. Some Unit test and mock test is done

## How to run this app
1. in the root folder of this project run these command:

    mvn package
    java -Xmx512m -jar target/sinorbis-0.0.1-SNAPSHOT.jar 

2. You could use test.html and some images for testing
## Other things

The interface of ImageStoreService is implemented by LocalImageStoreService and S3ImageStoreService. the former one is for testing in the Local file systerm and the second one is used in this application. The only way of this two is just the difference of guide input Stream
