package com.sinorbis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication
@EnableWebMvc
public class SinorbisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinorbisApplication.class, args);
	}
}
