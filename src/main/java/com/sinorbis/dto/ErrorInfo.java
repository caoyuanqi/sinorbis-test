package com.sinorbis.dto;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
/**
 * Show error informations to user
 * @author cao
 *
 */
public class ErrorInfo {


	String error;
	String usage;	 
	
}
