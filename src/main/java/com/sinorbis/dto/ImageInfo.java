package com.sinorbis.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * return the file name of image
 * @author cao
 *
 */
@Getter
@Setter
public class ImageInfo {

	String originalName;
	String small;
	String middle;
	String large;
}
