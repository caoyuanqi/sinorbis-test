package com.sinorbis.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * Show usage information to user
 * @author cao
 *
 */
public class Usage {
	
	String usage;
	String admin;
	
}
