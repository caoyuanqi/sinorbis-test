package com.sinorbis.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.sinorbis.dto.ImageInfo;
import com.sinorbis.exception.EmptyFileException;
import com.sinorbis.exception.UnsupportFile;

public interface ImageStoreService {

	List<ImageInfo> storeImages(MultipartFile[] files) throws EmptyFileException, UnsupportFile, IOException;
	ImageInfo storeImage(MultipartFile file) throws IOException ;

}
