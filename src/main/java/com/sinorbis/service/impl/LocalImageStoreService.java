package com.sinorbis.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sinorbis.dto.ImageInfo;
import com.sinorbis.exception.EmptyFileException;
import com.sinorbis.exception.UnsupportFile;
import com.sinorbis.service.ImageStoreService;
import com.sinorbis.util.ImageUtility;
import com.sinorbis.util.UniqueNameUtility;

import lombok.extern.log4j.Log4j;

/**
 * This service is create to test the image resizing service in local file system, 
 * it implyment the same interface and only difference is store way
 * @author cao
 *
 */

@Service
@Qualifier("local")
@Log4j
public class LocalImageStoreService implements ImageStoreService {

	@Value("${image.location}")
	private String fileLocation;

	@Autowired
	private UniqueNameUtility uniqueNameUtility;

	
	@Autowired
	private ImageUtility imageUtility;
	
	@Override
	public List<ImageInfo> storeImages(MultipartFile[] files) throws EmptyFileException, UnsupportFile, IOException {
		if(files.length == 0){
			throw new EmptyFileException();
		}
//		check the file type 
		for (MultipartFile file : files){
			if (!imageUtility.checkImageValidation(file.getOriginalFilename())){
				throw new UnsupportFile();
			}
		}
		List<ImageInfo> result = new ArrayList<ImageInfo>();
		for(MultipartFile file : files){
			result.add(storeImage(file));
		}
		return result;
	}
	

	@Override
	public ImageInfo storeImage(MultipartFile file) throws IOException {
		log.info("Store file with name " + file.getOriginalFilename());
		String fileName = file.getOriginalFilename();
		log.info("The orignal name of file is " + fileName);
		InputStream in = file.getInputStream(); 
		
//		get the inputStream from file
		String bigFileName = uniqueNameUtility.getUniqueName() + ".jpg";
		String middleFileName = uniqueNameUtility.getUniqueName() + ".jpg";
		String smallFileName = uniqueNameUtility.getUniqueName() + ".jpg"; 
//		deal with big file 
		InputStream largeIn = imageUtility.getLageImageStream(in);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		IOUtils.copy(largeIn, baos);
		byte[] bytes = baos.toByteArray(); // store into an array for deal with smaller and middle size image
		InputStream middleIn = imageUtility.getMiddleImageStream(new ByteArrayInputStream(bytes));
		InputStream smallIn = imageUtility.getSmallImageStream(new ByteArrayInputStream(bytes));
//		store the files 
		storeFile(smallIn, smallFileName);
		storeFile(middleIn, middleFileName);
		storeFile(new ByteArrayInputStream(bytes) ,bigFileName);
//		return the result
		ImageInfo info = new ImageInfo();
		info.setOriginalName(fileName);
		info.setLarge(bigFileName);
		info.setMiddle(middleFileName);
		info.setSmall(smallFileName);
		return info;
	}
	
	private void storeFile(InputStream in, String fileName) throws IOException{
		String folder = fileLocation + "/";
		log.info("Target file name is " + folder + fileName);
		File targetFile = new File(folder + fileName);
		FileUtils.copyInputStreamToFile(in, targetFile);
	}

}
