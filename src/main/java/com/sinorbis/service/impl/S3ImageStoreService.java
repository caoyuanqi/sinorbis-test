package com.sinorbis.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.sinorbis.dto.ImageInfo;
import com.sinorbis.exception.EmptyFileException;
import com.sinorbis.exception.UnsupportFile;
import com.sinorbis.service.ImageStoreService;
import com.sinorbis.util.ImageUtility;
import com.sinorbis.util.UniqueNameUtility;

import lombok.extern.log4j.Log4j;

/**
 * 	This the image store service version in S3 version. the only difference is the way to store images
 *
 * @author cao
 *
 */

@Service
@Qualifier("s3")
@Log4j
public class S3ImageStoreService implements ImageStoreService {


	@Autowired
	private UniqueNameUtility uniqueNameUtility;


	@Autowired
	private AmazonS3Client amazonS3Client;
	
	@Value("${cloud.aws.s3.bucket}")
	private String bucket;	
	
	@Autowired
	private ImageUtility imageUtility;

	/**
	 * 1. validate file type
	 * 2. store resized files
	 */
	@Override
	public List<ImageInfo> storeImages(MultipartFile[] files) throws EmptyFileException, UnsupportFile, IOException {
		if(files.length == 0){
			throw new EmptyFileException();
		}
//		check the file type 
		for (MultipartFile file : files){
			if (!imageUtility.checkImageValidation(file.getOriginalFilename())){
				throw new UnsupportFile();
			}
		}
		List<ImageInfo> result = new ArrayList<ImageInfo>();
		for(MultipartFile file : files){
			result.add(storeImage(file));
		}
		return result;
	}
	

	/**
	 * 1. store 3 types of resized file 
	 */
	@Override
	public ImageInfo storeImage(MultipartFile file) throws IOException {
		log.info("Store file with name " + file.getOriginalFilename());
		String fileName = file.getOriginalFilename();
		log.debug("The orignal name of file is " + fileName);
		InputStream in = file.getInputStream(); 
		
//		get the inputStream from file
		String bigFileName = uniqueNameUtility.getUniqueName() + ".jpg";
		String middleFileName = uniqueNameUtility.getUniqueName() + ".jpg";
		String smallFileName = uniqueNameUtility.getUniqueName() + ".jpg"; 
//		deal with big file 
		InputStream largeIn = imageUtility.getLageImageStream(in);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		IOUtils.copy(largeIn, baos);
		byte[] bytes = baos.toByteArray(); // store into an array for deal with smaller and middle size image
		InputStream middleIn = imageUtility.getMiddleImageStream(new ByteArrayInputStream(bytes));
		InputStream smallIn = imageUtility.getSmallImageStream(new ByteArrayInputStream(bytes));
//		store the files 
		storeFile(smallIn, smallFileName);
		storeFile(middleIn, middleFileName);
		storeFile(new ByteArrayInputStream(bytes) ,bigFileName);
//		return the result
		ImageInfo info = new ImageInfo();
		info.setOriginalName(fileName);
		info.setLarge(bigFileName);
		info.setMiddle(middleFileName);
		info.setSmall(smallFileName);
		return info;
	}

	
	
	/**
	 * Store the image to S3
	 * @param in
	 * @param fileName
	 * @throws IOException
	 */
	private void storeFile(InputStream in, String fileName) throws IOException{

		PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fileName, in, new ObjectMetadata());
		// why not allow me to access :(
//		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);

		PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);
		log.debug("Get from aws s3" + putObjectResult);
		IOUtils.closeQuietly(in);	

		//FileUtils.copyInputStreamToFile(in, targetFile);
	}

}
