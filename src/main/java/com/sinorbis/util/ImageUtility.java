package com.sinorbis.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * Deal with imageStream get from multipart file and get another Stream to smaller image
 * @author cao
 *
 */
public interface ImageUtility {


	
	InputStream getImageStream(InputStream input, int width, int height) throws IOException;
	
	InputStream getSmallImageStream(InputStream input) throws IOException;
	
	InputStream getMiddleImageStream(InputStream input) throws IOException;
	
	InputStream getLageImageStream(InputStream input) throws IOException;
	
	boolean checkImageValidation(String fileName);
}
