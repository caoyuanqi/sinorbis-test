package com.sinorbis.util;

/**
 * get unique name
 * @author cao
 *
 */
public interface UniqueNameUtility {

	String getUniqueName();
}
