package com.sinorbis.util.impl;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sinorbis.util.ImageUtility;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
@Getter
@Setter
@Component
@Log4j
public class ImageUtilityImpl implements ImageUtility{


	@Value("${image.small.width}")
	private int smallWidth;
	@Value("${image.small.height}")
	private int smallHeight;
	@Value("${image.middle.width}")
	private int middleWidth;
	@Value("${image.middle.height}")
	private int middleHeight;
	@Value("${image.large.width}")
	private int largeWidth;
	@Value("${image.large.height}")
	private int largeHeight;

	
	private static final String IMAGE_PATTERN =    "([^\\s]+(\\.(?i)(jpg|png|gif|bmp|jpeg))$)";
	
	
	
@Override
/**
 * borrowed from https://gist.github.com/werbth/4b1cf7567657cd64c71eb08099544f35
 */
public	InputStream getImageStream(InputStream inputStream, int width, int height) throws IOException {
	log.info("Convert input stream into " + width + " px " + height+" px");
	log.info("Input stream is " + inputStream);
	BufferedImage sourceImage = ImageIO.read(inputStream);
	log.info("The input image is " + sourceImage);
	Image thumbnail = sourceImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null),
            thumbnail.getHeight(null),
            BufferedImage.TYPE_INT_RGB);
    	bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	ImageIO.write(bufferedThumbnail, "jpg", baos);
    	return new ByteArrayInputStream(baos.toByteArray());
}


@Override
public InputStream getSmallImageStream(InputStream input) throws IOException {
	return getImageStream(input, smallWidth, smallHeight);
}

@Override
public InputStream getMiddleImageStream(InputStream input) throws IOException {
	return getImageStream(input, middleWidth, middleHeight);
}

@Override
public InputStream getLageImageStream(InputStream input) throws IOException {
	return getImageStream(input, largeWidth, largeHeight);
}

@Override
public boolean checkImageValidation(String fileName) {
	Matcher matcher;
	Pattern pattern = Pattern.compile(IMAGE_PATTERN);        
	matcher = pattern.matcher(fileName);
	return matcher.matches();
	}	
}
