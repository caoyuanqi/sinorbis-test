package com.sinorbis.util.impl;

import java.util.UUID;

import org.springframework.stereotype.Component;

import com.sinorbis.util.UniqueNameUtility;
@Component
public class UUIDUniqueNameUtility implements UniqueNameUtility {

	/**
	 * return uuid without "-" as file name 
	 */
	@Override
	public String getUniqueName() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

}
