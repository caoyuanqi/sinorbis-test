package com.sinorbis.web;


import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sinorbis.dto.ErrorInfo;
import com.sinorbis.dto.ImageInfo;
import com.sinorbis.dto.Usage;
import com.sinorbis.exception.EmptyFileException;
import com.sinorbis.exception.UnsupportFile;
import com.sinorbis.service.ImageStoreService;

@RestController
public class ImageController {

	@Autowired
	@Qualifier("s3")
	private ImageStoreService imageService;
	
/**
 *  Get usage information
 * @return
 */
	@GetMapping("/")
	Usage getUsage(){
		Usage u = new Usage();
		u.setUsage("Post files list to this url and get a list of resized images name in s3 ");
		u.setAdmin("John : admin@admin.com");
		return u;
	}

	/**
	 * Post files to images parameter
	 * @param files
	 * @return
	 * @throws EmptyFileException
	 * @throws UnsupportFile
	 * @throws IOException
	 */
	@PostMapping("/")
	List<ImageInfo> postStoreImage(@RequestParam(name = "images", required = true) MultipartFile[ ] files) throws EmptyFileException, UnsupportFile, IOException{
		return imageService.storeImages(files);
	}
	

	@ExceptionHandler(value = UnsupportFile.class)
	ErrorInfo noSupportFileErro(){
		ErrorInfo info = new ErrorInfo();
		info.setError("Unsupport file, we only accept image files");
		info.setUsage("Please post images files as paramater name: images");
		return info; 
	}

	@ExceptionHandler(value = EmptyFileException.class)
	ErrorInfo noFileUploadError(){
		ErrorInfo info = new ErrorInfo();
		info.setError("You should upload at least 1 file. ");
		info.setUsage("Please post image files as paramater name : images");
		return info;
	}

@ExceptionHandler(value = {IOException.class, Exception.class})	
	ErrorInfo otherException(){
		ErrorInfo info = new ErrorInfo();
		info.setError("We meet some inner erro, please contact admin: admin@admin.com");
		info.setUsage("Please post image files as paramater name : images");
		return info;
	}
}
