package com.sinorbis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import com.sinorbis.dto.ImageInfo;
import com.sinorbis.service.ImageStoreService;
import com.sinorbis.web.ImageController;


import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;  
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;  

import static org.mockito.Mockito.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class SinorbisApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
    private WebApplicationContext webApplicationContext;

	@Mock
	private ImageStoreService imageService;
	

	@InjectMocks
	ImageController controller;

	
	
    @Test
    /**
     * Test if the files type is ok
     * @throws Exception
     */
    public void testExceptionMessage() throws Exception {
    	// Test wrong image file 
    	MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build(); 
        MockMultipartFile firstFile = new MockMultipartFile("images", "filename.txt", "text/plain", "some xml".getBytes());
        mockMvc.perform(fileUpload("/").file(firstFile))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.error").value("Unsupport file, we only accept image files"));  
        ; 

    }
    
    @Test
    /**
     * Unit test the controller
     * @throws Exception
     */
    public void testController() throws Exception {
    	// Test wrong image file 
    	MockMvc mockMvc2 = MockMvcBuilders.standaloneSetup(controller).build();
        MockMultipartFile file = new MockMultipartFile("images", "filename.jpg", "image/jpg", "some xml".getBytes());
        List<ImageInfo> result = new ArrayList<ImageInfo>();
        ImageInfo info = new ImageInfo();
        info.setOriginalName("filename.jpg");
        info.setLarge("larget");
        info.setMiddle("middle");
        info.setSmall("small");
        result.add(info);
        when(imageService.storeImages(any(MultipartFile[].class))).thenReturn(result);
        mockMvc2.perform(fileUpload("/").file(file))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].small").value("small"));  
        ; 

    }
}
