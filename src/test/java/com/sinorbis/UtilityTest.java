package com.sinorbis;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.sinorbis.util.UniqueNameUtility;
import com.sinorbis.util.impl.UUIDUniqueNameUtility;

public class UtilityTest {


	private UniqueNameUtility uniqueNameUtility;

	@Before
	public void setUp(){
		uniqueNameUtility = new UUIDUniqueNameUtility();
	}
	
	
	@Test
	public void test() {
		assertFalse("Unique name should not contain dash mark",uniqueNameUtility.getUniqueName().contains("-"));
		
	}

}
